var firstRun=false;

chrome.runtime.onInstalled.addListener(function(e){

  chrome.storage.sync.get({
    timeframe:2
  },function(items){
    
    timeframe=parseInt(items.timeframe);
    chrome.alarms.create('refreshFeed',{when:Date.now(),periodInMinutes:timeframe});
    console.log('Installed and alarm created')
    firstRun=true;
    openUrl(chrome.extension.getURL('options.html'));

  });

});

chrome.runtime.onMessage.addListener(function(timeframe,sender,sendResponse){

  timeframe=parseInt(timeframe);
  console.log(timeframe);
  chrome.alarms.clearAll();
  chrome.alarms.create('refreshFeed',{when:Date.now(),periodInMinutes:timeframe});
  console.log("Alarm created");

});

function sendResponse(){};

chrome.alarms.onAlarm.addListener(function(e){

chrome.storage.sync.get({
  toBeNotified:true,
  topic:'/en/read'  
},function(items){

  if(items.toBeNotified==false){
    return;
  }
  else{
    fetch_url='https://www.inshorts.com'+items.topic
    fetch_feed(fetch_url,function(response){
        display_stories(response);
    });
  }
});   
  chrome.alarms.get("refreshFeed",function(alarm){console.log("Period in minutes: "+alarm.periodInMinutes);});

});


function display_stories(feed_data){
  if(firstRun==true){
    console.log('First run')
    $(feed_data).find('.news-card').each(function(){

      var fullSourceUrl="https://www.inshorts.com"+$(this).find('.news-card-title a').attr('href');
      try{
        var id=fullSourceUrl.split('-').pop();
      }
      catch(err){
        console.log("Motherfucker")
        return;
      }
      localStorage.setItem(id,'showed')

    });
   firstRun=false; 
  }

  else{

    var numberOfNewsShowed=0
    $(feed_data).find('.news-card').each(function(){

      var fullSourceUrl="https://www.inshorts.com"+$(this).find('.news-card-title a').attr('href');
      try{
        var id=fullSourceUrl.split('-').pop();
      }
      catch(err){
        console.log("Hahalol")
        return;      
      }

      if(localStorage.hasOwnProperty(id)){ //True. It is shown
        return;
      } 

      numberOfNewsShowed+=1;
      if(numberOfNewsShowed>3){  //If number of unshowed notifications are less than 3, show. Else, hide them.
        localStorage.setItem(id,'showed')
        console.log('Stopper hit')
        return;
      }

      //Else, not shown. Go ahead and create the notification
      //var imgUrl=$(this).find('.news-card-image img').attr('src').split('?')[0]+"?resize=1200px:800px";
      //var imgUrl=$(this).find('.news-card-image').attr('style').split('\'')[1].split('?')[0]+"?resize=1200px:800px";
      var fullSourceUrl=$(this).find('.news-card-footer div a').attr('href'); 
      var headline=$(this).find('.news-card-title a span').text(); 
      var newsBody=$(this).find('.news-card-content div:first').text(); 
      var sourceName=$(this).find('.news-card-footer div a').text();  
      var shorts='https://www.inshorts.com/'+$(this).find('.news-card-title a').attr('href').split('/').slice(2).join('/')
      var date=new Date($(this).find('.time').attr('content'))
      var dateTimeString=formatAMPM(date)+" "+date.toDateString()

      var options={type:'basic',
        title:headline,
        message:"",
        contextMessage:dateTimeString,
        buttons:[
        {title:'more at '+sourceName,iconUrl:'img/chrome.png'},
        {title:'Cancel',iconUrl:'img/close.png'}
        ],
        iconUrl:"img/favicon.png",
        isClickable:true
      }

      chrome.notifications.onClicked.addListener(registerClickedListener);
      chrome.notifications.onClosed.addListener(registerCloseListener);
      chrome.notifications.onButtonClicked.addListener(registerButtonListener);

      chrome.notifications.create(id,options,function(){
        localStorage.setItem(id,'showed')
      });

      function registerClickedListener(notifId){ //User clicked the notification

        if(notifId!=id){
          return;
        }
        openUrl(shorts)
        chrome.notifications.clear(id);
      }

      function registerButtonListener(notifId,buttonIndex){
        if(id!=notifId){
          return;
        }

        if(buttonIndex==0){ //Opened in browser
          openUrl(fullSourceUrl)
          chrome.notifications.clear(id);                    
        }
        else if(buttonIndex==1){ //Hit close
          chrome.notifications.clear(id);
        }

      }

      function registerCloseListener(notifId){
        if(notifId!=id){
          return;
        }
        chrome.notifications.onClicked.removeListener(registerClickedListener);
        chrome.notifications.onButtonClicked.removeListener(registerButtonListener);  
        chrome.notifications.onClosed.removeListener(registerCloseListener);
      }
 

    });

  }

}

function openUrl(url){ //TODO: If on other screen, open chrome
  chrome.windows.getCurrent(function(currentWindow) {
    if (currentWindow != null) {
      return chrome.tabs.create({
        'url': url
      });
    } else {
      return chrome.windows.create({
        'url': url,
        'focused': true
      });
    }
  });
}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}