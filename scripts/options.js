function save_options(){
	var favtopic=document.getElementById('topic').value;
	var notify=document.getElementById('notify').checked;
	var timeframe=document.getElementById('timeframe').value;

	chrome.runtime.sendMessage(timeframe);

	chrome.storage.sync.set({
		topic:favtopic,
		toBeNotified:notify,
		timeframe:timeframe
	},function(){

	    var status = document.getElementById('status');
	    status.textContent = 'Options saved.';
	    setTimeout(function() {
	      status.textContent = '';
	      window.close();
	    }, 1500);

	});
}

function restore_options(){

	chrome.storage.sync.get({
		topic:'/en/read',
		toBeNotified:true,
		timeframe:2
	},function(items){
		document.getElementById('topic').value=items.topic;
		document.getElementById('notify').checked=items.toBeNotified;
		document.getElementById('timeframe').value=items.timeframe;
	});

}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);
