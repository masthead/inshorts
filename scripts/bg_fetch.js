function fetch_feed(url, callback) {
  var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(data) {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          var data = xhr.responseText;
          callback(data);
        } else {
          callback(null);
        }
      }
    }
    xhr.open('GET', url, true);
    xhr.send();
}

function open_item(url) {
  chrome.tabs.create({url: url});
  chrome.browserAction.setBadgeText({text:''});
}